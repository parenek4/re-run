import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session/session.service';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
   
    constructor(private http: HttpClient, private session: SessionService) { }

        getSurveys(): Promise<any>{
            return this.http.get(`${environment.apiUrl}/v1/api/surveys`,{
                headers:{
                    'Authorization': 'Bearer' + this.session.get().token
                }
            }).toPromise()
        }
    getSurveyById(surveyId): Promise<any>{
        return this.http.get(`${environment.apiUrl}/v1/api/surveys/${surveyId}`).toPromise();
    }

  }
  

