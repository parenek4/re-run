import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 
  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [ Validators.required, Validators.minLength(5)]),
  })

  isLoading: boolean = false;
  registerError: string;

  constructor(private auth: AuthService, private session: SessionService, private router:Router) { 
    if (this.session.get() !== false){
      this.router.navigateByUrl('/dashboard');
    }
  }

  ngOnInit(): void {
  }

  get username () {
    return this.registerForm.get('username')
  }
  get password() {
    return this.registerForm.get('password')
  }
 async  onRegisterClicked(){
   this.registerError = '';
   try {
     this.isLoading=true;

     const result: any = await this.auth.register(this.registerForm.value);

     if(result.status < 400) {
       this.session.save({
         token: result.data.token,
         username: result.data.user.username
       });
       this.router.navigateByUrl('/dashboard')
     }
     
     

   }catch(e){
     console.error(e.error); 
     this.registerError = e.error.error;
     
   }finally{
   this.isLoading=false;
   }

  };

}
