import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-survey-list-item',
  templateUrl: './survey-list-item.component.html',
  styleUrls: ['./survey-list-item.component.css']
})
export class SurveyListItemComponent implements OnInit {
  
  @Input() survey;

  constructor() { }

  ngOnInit(): void {
  }

}
