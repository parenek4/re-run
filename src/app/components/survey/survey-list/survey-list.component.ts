import { Component, OnInit } from '@angular/core';
import { SurveyService } from 'src/app/services/survey/survey.service';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {
  surveys: any[] = [];

  constructor(private surveyService: SurveyService ) { }

  async ngOnInit()  {
    try {
      const result: any = await this.surveyService.getSurveys();
      this.surveys = result.data || [];
      console.log('RESULT', result);
      

    }
    catch(e) {
      console.error('ERROR:', e)
  }

}
}
